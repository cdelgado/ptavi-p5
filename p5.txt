***** Análisis de una sesión SIP

Se ha capturado una sesión SIP con Ekiga (archivo sip.cap.gz), que se puede abrir con Wireshark. Se pide rellenar las cuestiones que se plantean en este guión en el fichero p5.txt que encontrarás también en el repositorio.

  * Observa que las tramas capturadas corresponden a una sesión SIP con Ekiga, un cliente de VoIP para GNOME. Responde a las siguientes cuestiones:
    * ¿Cuántos paquetes componen la captura? 954 paquetes.
    * ¿Cuánto tiempo dura la captura? Dura 56.149345 segundos.
    * ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes? Esta efectuada desde la IP 192.168.1.34. Es privada de clase C, van de 192.168.0.0 a 192.168.255.255,al estar dentro de este rango,es privada.

  * Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el porcentaje del tráfico correspondiente al protocolo TCP y UDP.
    * ¿Cuál de los dos es mayor? ¿Tiene esto sentido si estamos hablando de una aplicación que transmite en tiempo real? El porcentaje de UDP es mayor que el de TCP (96.2%>2.1% de paquetes). Tiene sentido porque RTP se encapsula sobre UDP y TCP no sirve para aplicaciones en tiempo real.
    * ¿Qué otros protocolos podemos ver en la jerarquía de protocolos? ¿Cuales crees que son señal y cuales ruido? STUN, H.261, RTCP, DNS, HTTP, SIP, RTP, ICMP y ARP. Son señal STUN, SIP, H.261, RTP, RTCP y HTTP. Son ruido DNS, ICMP y ARP

  * Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, con una llamada entremedias.
    * Filtra por sip para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos? En el segundo 7 se envian 6 paquetes, en el 14 se envian 3, en el 16 se hay 4, en el 38 hay 4, en el 39 hay 4, y en el 55 hay 4.
    * Y los paquetes con RTP, ¿cuándo se envían? Se empiezan a enviar en el segundo 17, al iniciar la sesion SIP y se dejan de enviar en el segundo 38, al cerrar la sesión.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Analiza las dos primeras tramas de la captura.
    * ¿Qué servicio es el utilizado en estas tramas? DNS.
    * ¿Cuál es la dirección IP del servidor de nombres del ordenador que ha lanzado Ekiga? La dirección IP es 80.58.61.250.
    * ¿Qué dirección IP (de ekiga.net) devuelve el servicio de nombres? Devuelve la dirección 86.64.162.35.

  * A continuación, hay más de una docena de tramas TCP/HTTP.
    * ¿Podrías decir la URL que se está pidiendo? Pide ekiga.net/ip/, como se puede observar en el paquete numero 8.
    * ¿Qué user agent (UA) la está pidiendo? Ekiga.
    * ¿Qué devuelve el servidor? Le devuelve un 200 OK y la dirección IP: 83.36.48.212.
    * Si lanzamos el navegador web, por ejemplo, Mozilla Firefox, y vamos a la misma URL, ¿qué recibimos? ¿Qué es, entonces, lo que está respondiendo el servidor? Recibimos la dirección 79.147.172.221, nuestra direccion ip.

  * Hasta la trama 45 se puede observar una secuencia de tramas del protocolo STUN.
    * ¿Por qué se hace uso de este protocolo? Para diferenciar los tipos de Nat y para que el cliente pueda encontrar su dirección IP.
    * ¿Podrías decir si estamos tras un NAT o no? Estamos tras un Nat porque en el 45 tenemos simple traversal of UDP through NAT y porque Stun permite a clientes Nat encontrar su direcció pública.

  * La trama 46 es la primera trama SIP. En un entorno como el de Internet, lo habitual es desconocer la dirección IP de la otra parte al realizar una llamada. Por eso, todo usuario registra su localización en un servidor Registrar. El Registrar guarda información sobre los usuarios en un servidor de localización que puede ser utilizado para localizar usuarios.
    * ¿Qué dirección IP tiene el servidor Registrar? La dirección IP es 86.64.162.35.
    * ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP? Se envian al puerto 5060.
    * ¿Qué método SIP utiliza el UA para registrarse? Se utiliza el método REGISTER.
    * Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA? INVITE, ACK, OPTIONS, BYE, CANCEL, NOTIFY, REFER, MESSAGE.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Fijémonos en las tramas siguientes a la número 46:
    * ¿Se registra con éxito en el primer intento? No, se puede observar en la trama 50 donde el servidor SIP ha contestado con error 401 unauthorized.
    * ¿Cómo sabemos si el registro se ha realizado correctamente o no? Si se registra correctamente aparece un 200 OK.
    * ¿Podrías identificar las diferencias entre el primer intento y el segundo de registro? (fíjate en el tamaño de los paquetes y mira a qué se debe el cambio) En el segundo se ha inlcuido el campo authentication (la autorización).
    * ¿Cuánto es el valor del tiempo de expiración de la sesión? Indica las unidades. 3600 segundos.

  * Una vez registrados, podemos efectuar una llamada. Vamos a probar con el servicio de eco de Ekiga que nos permite comprobar si nos hemos conectado correctamente. El servicio de eco tiene la dirección sip:500@ekiga.net. Veamos el INVITE de cerca.
    * ¿Puede verse el nombre del que efectúa la llamada, así como su dirección SIP? Si, el nombre es Gregorio Robles y la direccion SIP es grex@ekiga.net, esto se puede observar en el campo from.
    * ¿Qué es lo que contiene el cuerpo de la trama? ¿En qué formato/protocolo está? Contiene la informacion del protocolo de inicio de sesión(Session Description Protocol).
    * ¿Tiene éxito el primer intento? ¿Cómo lo sabes? No, porque me responde con un 407 proxy authentication required, es decir , no esta autorizado.
    * ¿En qué se diferencia el segundo INVITE más abajo del primero? ¿A qué crees que se debe esto? En el segundo INVITE se añadió proxy authorization. Se debe a que es obligatorio añadirlo.


  * Una vez conectado, estudia el intercambio de tramas.
    * ¿Qué protocolo(s) se utiliza(n)? ¿Para qué sirven estos protocolos? Se utiliza RTP(sirve para el trafico multimedia de audio), H.261(sirve para el trafico multimedia de video) y y DNS(almacena informacion de control)..
    * ¿Cuál es el tamaño de paquete de los mismos? Los paquetes de RTP tienen una tamaño de 214 bytes y los H.261 de 1078 como máximo.
    * ¿Se utilizan bits de padding? No, aparece padding=False.
    * ¿Cuál es la periodicidad de los paquetes (en origen; nota que la captura es en destino)? La periodicidad es de aproximadamente 20ms.
    * ¿Cuántos bits/segundo se envían? Al ser G.711 se envían 54kbps.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Vamos a ver más a fondo el intercambio RTP. En Telephony hay una opción RTP. Empecemos mirando los flujos RTP.
    * ¿Cuántos flujos hay? ¿por qué? Hay 2 flujos, uno para audio y otro para video.
    * ¿Cuántos paquetes se pierden? No se pierde ninguno.
    * ¿Cuál es el valor máximo del delta? ¿Y qué es lo que significa el valor de delta? El valor máximo es 1290.444 ms en rtp y 1290.479 ms en H.261. El valor de la delta significa el retardo respecto a cuando deberia haber llegado.
    * ¿Cuáles son los valores de jitter (medio y máximo)? ¿Qué quiere decir eso? ¿Crees que estamos ante una conversación de calidad? En H.261 el valor medio es 153.240 ms y el maximo 183.096 ms. En RTP el valor maximo es 119.635 ms y el medio 42.500 ms. No es una conversacion de calidad porque el valor del jitter es muy alto. Para ser de calidad el jitter máximo deberia ser menos de 100ms.

  * Elige un paquete RTP de audio. Analiza el flujo de audio en Telephony -> RTP -> Stream Analysis.
    * ¿Cuánto valen el delta y el jitter para el primer paquete que ha llegado? En el primer paquete delta vale 0 y el jitter vale 0.
    * ¿Podemos saber si éste es el primer paquete que nos han enviado? Si, porque el valor del jitter es cero.
    * Los valores de jitter son menores de 10ms hasta un paquete dado. ¿Cuál? Son menores hasta el paquete 247.
    * ¿A qué se debe el cambio tan brusco del jitter? Se debe a que el paquete ha tardado mucho en llegar.
    * ¿Es comparable el cambio en el valor de jitter con el del delta? ¿Cual es más grande? Es comparable, ambos han subido bruscamente sobretodo el delta y asi se compensan.

  * En Telephony selecciona el menú VoIP calls. Verás que se lista la llamada de voz IP capturada en una ventana emergente. Selecciona esa llamada y pulsa el botón Play Streams.
    * ¿Cuánto dura la conversación? 24 segundos.
    * ¿Cuáles son sus SSRC? ¿Por qué hay varios SSRCs? ¿Hay CSRCs? Sus SSRC son 0xbf4afd37 y 0x43306582. Hay varios para evitar colisiones. No hay CSRCs.

  * Identifica la trama donde se finaliza la conversación.
    * ¿Qué método SIP se utiliza? BYE.
    * ¿En qué trama(s)? En la 924, 925, 927 y 933.
    * ¿Por qué crees que se envía varias veces? Se envia tantas veces porque hasta que no recibe un 200 OK de vuelta, no puede cerrar la sesión.

  * Finalmente, se cierra la aplicación de VozIP.
    * ¿Por qué aparece una instrucción SIP del tipo REGISTER? Para cerrar la conexion SIP.
    * ¿En qué trama sucede esto? Sucede en la 950, pero hay un error y se vuelve a mandar en la 952.
    * ¿En qué se diferencia con la instrucción que se utilizó con anterioridad (al principio de la sesión)? Antes el campo expires era 3600 y ahora vale cero. Al ser cero,se borra el usuario.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

***** Captura de una sesión SIP

  * Dirígete a la web de Linphone (https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
  * Lanza linphone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú ``Ayuda'' y seleccionar ``Asistente de Configuración de Cuenta''. Al terminar, cierra completamente linphone.

  * Captura una sesión SIP de una conversación con el número SIP sip:music@sip.iptel.org. Recuerda que has de comenzar a capturar tramas antes de arrancar Ekiga para ver todo el proceso.

  * Observa las diferencias en el inicio de la conversación entre el entorno del laboratorio y el del ejercicio anterior:
    * ¿Se utilizan DNS y STUN? ¿Por qué? Aparecen DNS (obtener ip servidor) y STUN (al ser una red privada y asi obtener mas informacion).
    * ¿Son diferentes el registro y la descripción de la sesión? Son diferentes, un tiene la informacion y el otro la descripcion de registro.

  * Identifica las diferencias existentes entre esta conversación y la conversación anterior:
    * ¿Cuántos flujos tenemos? Tenemos 2 flujos.
    * ¿Cuál es su periodicidad? Su periodicidad es 20ms.
    * ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter? En el flujo 1: Max Delta = 26.566, Max Jitter = 1.326 y Mean Jitter = 0.338.
	En el flujo 2: Max Delta = 40.870, Max Jitter = 8.586 y Mean Jitter = 5.321.
    * ¿Podrías reproducir la conversación desde Wireshark? ¿Cómo? Comprueba que poniendo un valor demasiado pequeño para el buffer de jitter, la conversación puede no tener la calidad necesaria. 
    Se podria reproducir desde Telephony -->  VoIP calls --> play streams.
    * ¿Sabrías decir qué tipo de servicio ofrece sip:music@iptel.org? Es un servicio de musica.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Filtra por los paquetes SIP de la captura y guarda *únicamente* los paquetes SIP como p5.pcapng. Abre el fichero guardado para cerciorarte de que lo has hecho bien. Deberás añadirlo al repositorio.

[Al terminar la práctica, realiza un push para sincronizar tu repositorio GitLab]

IMPORTANTE: No olvides rellenar el test de la práctica 5 en el Aula Virtual de la asignatura.
